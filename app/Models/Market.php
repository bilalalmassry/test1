<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Market extends Model
{
    use HasFactory;

    public function myUser(){
        return $this->belongsTo(User::class,'user_id')->get();
    }
    protected $appends = ['user'];

    public function  getUserAttribute(): \Illuminate\Database\Eloquent\Collection
    {
        return $this->belongsTo(User::class,'user_id')->get();
    }
}
