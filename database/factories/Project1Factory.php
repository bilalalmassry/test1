<?php

namespace Database\Factories;

use App\Models\Project1;
use Illuminate\Database\Eloquent\Factories\Factory;

class Project1Factory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Project1::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
